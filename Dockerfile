# base image
FROM node:12.2.0-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY . ./

RUN npm install --silent
RUN npm install -g typescript@2.7.2
RUN npm install react-scripts

# start app
CMD ["npm", "start"]

EXPOSE 3000

#  docker run -it -p 7005:3000 -t <image id>
